using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzEventsDelegatesExercise
{
    class Program
    {
        //create 2 events: DividedBy3 and Divided by 5
        public class DividedBy3and5EventArgs : EventArgs
        {
            public int Number { get; set; }

            public DividedBy3and5EventArgs(int number)
            {
                Number = number;
            }
        }

        //add corresponsing methods(event args + event handler) which prints:
        //    if dividded by 3 - Fizz
        //    if devided by 5- Buzz

        static void NumberDividedBy3(object sender, DividedBy3and5EventArgs d)
        {
            Console.WriteLine($"Number sended by: {sender}");
            Console.WriteLine($"{d.Number}: Fizz");
        }

        static void NumberDividedBy5(object sender, DividedBy3and5EventArgs d)
        {
            Console.WriteLine($"Number sended by: {sender}");
            Console.WriteLine($"{d.Number}: Buzz");
        }

        static void NumberDividedBy3and5(object sender, DividedBy3and5EventArgs d)
        {
            Console.WriteLine($"Number sended by: {sender}");
            Console.WriteLine($"{d.Number}: FizzBuzz");
        }

        private static event EventHandler<DividedBy3and5EventArgs> InvocationMethodsList;

        //create a function which runs from 1 to 20 and print number to screen
        //    if the number can be divided by 3 without reminder - invoke the DividedBy3
        //    if the number can be divided by 5 without reminder - invoke the DividedBy5
        private static void RunFrom1To20(int number)
        {
            if (InvocationMethodsList != null)
            {
                for (int i = 1; i <= number; i++)
                {
                    if (i % 5 == 0 & i % 3 == 0)
                    {
                        NumberDividedBy3and5("Divided By 3 and 5", new DividedBy3and5EventArgs(i));
                    }
                    else if (i % 3 == 0)
                    {
                        NumberDividedBy3("Divided By 3", new DividedBy3and5EventArgs(i));
                    }
                    else if (i % 5 == 0)
                    {
                        NumberDividedBy5("Divided By 5", new DividedBy3and5EventArgs(i));
                    }
                    else
                        Console.WriteLine($"{i}: {i}");
                }
            }



        }
        static void Main(string[] args)
        {
            InvocationMethodsList += NumberDividedBy3;
            InvocationMethodsList += NumberDividedBy5;
            InvocationMethodsList += NumberDividedBy3and5;

            RunFrom1To20(20);
        }
    }
}
