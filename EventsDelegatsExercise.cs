using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventsDelegatsExercise
{
    class Program
    {
        // 1. TODO:
        //take the code of real_event(chat)
        //add SlimEncoder
        //fire the SlimEncoder
        //add inside VideoEncoderEventArgs time of video as int, add it to each event you fire
        //   also print the video size in the functions of SMS and Email
        //add new function which upload the video into the cloud(same like email and sms function)
        //    no += this function and try to see if it works    

        //add inside VideoEncoderEventArgs time of video as int, add it to each event you fire
        //   also print the video size in the functions of SMS and Email
        public class VideoEncoderEventArgs : EventArgs
        {
            public static int count = 0;
            public string VideoName { get; set; }
            public VideoEncoderEventArgs()
            {
                count++;
            }


        }

        //public delegate void Func_Void_Arg_String(string s);

        static void SendEmailAfterEncoding(object sender, VideoEncoderEventArgs e)
        {
            Console.WriteLine($"Email: encoded by {sender}...");
            Console.WriteLine($"Email Header: Video Body: {e.VideoName} successfully encoded");
            Console.WriteLine($"Email: Video size {e.VideoName.Length}");
        }

        static void SendSmsAfterEncoding(object sender, VideoEncoderEventArgs e)
        {
            Console.WriteLine($"SMS: encoded by {sender}...");
            Console.WriteLine($"SMS: -- Body: {e.VideoName} successfully encoded --");
            Console.WriteLine($"SMS: Video size {e.VideoName.Length}");
        }
        //add new function which upload the video into the cloud(same like email and sms function)
        //    no += this function and try to see if it works
        static void UplodVideoIntoCloudEncoding(object sender, VideoEncoderEventArgs e)
        {
            Console.WriteLine($"Uplod: uploded by {sender}...");
            Console.WriteLine($"Uplod: -- Body: {e.VideoName} successfully uploded --");
            Console.WriteLine($"Uplod: Video size {e.VideoName.Length}");
        }

        //private static Action<object, VideoEncoderEventArgs> invocationMethodsList;
        private static event EventHandler<VideoEncoderEventArgs> invocationMethodsList;

        private static void MpgVideoEncoding(string videoName)
        {
            Console.WriteLine($"Encoding video {videoName}");
            Thread.Sleep(3000);

            if (invocationMethodsList != null)
            {
                invocationMethodsList("Mpeg encoder",
                    new VideoEncoderEventArgs { VideoName = videoName }); // fire all registered methods
            }

        }

        private static void Mp4VideoEncoding(string videoName)
        {
            Console.WriteLine($"Encoding video {videoName}");
            Thread.Sleep(3000);

            if (invocationMethodsList != null)
            {
                invocationMethodsList("Mp4 encoder",
                    new VideoEncoderEventArgs { VideoName = videoName });// fire all registered methods
            }

        }

        //add SlimEncoder
        private static void SlimVideoEncoding(string videoName)
        {
            Console.WriteLine($"Encoding video {videoName}");
            Thread.Sleep(3000);

            if (invocationMethodsList != null)
            {
                invocationMethodsList("Slim encoder",
                    new VideoEncoderEventArgs { VideoName = videoName });// fire all registered methods
            }

        }
        static void Main(string[] args)
        {
            // +=

            invocationMethodsList += SendEmailAfterEncoding;
            invocationMethodsList += SendSmsAfterEncoding;
            Mp4VideoEncoding("Batman movie...");

            invocationMethodsList -= SendEmailAfterEncoding;
            // this could only be done within the class
            // which holds the delegate as member
            //invocationMethodsList = null;
            //invocationMethodsList.Invoke(null, new VideoEncoderEventArgs { VideoName = "bla bla bla)" });
            MpgVideoEncoding("Superman movie...");

            //fire the SlimEncoder
            invocationMethodsList += SendEmailAfterEncoding;
            invocationMethodsList += SendSmsAfterEncoding;
            invocationMethodsList += UplodVideoIntoCloudEncoding;
            SlimVideoEncoding("Need For Speed...");

        }

    }
}
